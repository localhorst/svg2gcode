# svg2gcode

Convert vector images (SVG) to gcode for usage with a laser plotter. 

Based on the vector to gcode implementation from [Vishal Patil](https://github.com/vishpat/svg2gcode)

# Requirements

`pip install inkex`

# Usage

`clear && cat test_data/10mmx10mm.svg | python3 svg2gcode.py > test_data/test.gcode`


## Fix for Pronterface
`nano /usr/lib64/python3.8/site-packages/printrun/gl/libtatlin/actors.py`
replace content in lines 442 to 447 with:
```
        prev_move_normal_x = 0
        prev_move_normal_y = 0
        prev_move_angle = 0
        prev_pos = (0, 0, 0)
        prev_gline = None
        layer_idx = 0
```