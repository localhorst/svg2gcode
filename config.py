import argparse
import sys

class Config:

    """G-code emitted at the start of processing the SVG file"""
    preamble = "G90 ;Absolute programming\nG21 ;Programming in millimeters (mm)\nM5  ;Disable laser\n"

    """G-code emitted at the end of processing the SVG file"""
    postamble = "G1 X0.0 Y0.0; Display printbed\nM02 ;End of program\n"

    gcode_file_path = " "
    svg_file_path = " "
    feedrate = 0
    moverate = 0
    passes = 0
    laserpower = 0.0
    bedsizex = 0
    bedsizey = 0
    smoothness = 0.0

    def parse_arguments(self):
        parser = argparse.ArgumentParser(description='Generate gcode from vector graphics.')

        parser.add_argument("-i", "--input", dest='inputfile', metavar='image.svg', help="path to vector graphic file", required=True)
        parser.add_argument("-o", "--output", dest='outputfile', metavar='data.gcode', help="path to file for generated gcode", required=True)

        parser.add_argument("-fr", "--feedrate", dest='feedrate', metavar=300, default=300, help="rate while laser is on", required=False)
        parser.add_argument("-mr", "--moverate", dest='moverate', metavar=1200, default=1200, help="rate while laser is off", required=False)

        parser.add_argument("-p", "--passes", dest='passes', metavar=1, default=1, help="number of passes (for deeper cutting)", required=False)
        parser.add_argument("-lp", "--laserpower", dest='laserpower', metavar="0%", default="0%", help="laser power in %%", required=False)

        parser.add_argument("-bx", "--bedsizex", dest='bedsizex', default="376", help="x size of bed in mm", required=False)
        parser.add_argument("-by", "--bedsizey", dest='bedsizey', default="315", help="y size of bed in mm", required=False)

        parser.add_argument("-s", "--smoothness", dest='smoothness', metavar=0.2, default=0.2, help="Used to control the smoothness/sharpness of the curves.\nSmaller the value greater the sharpness.\nMake sure the value is greater than 0.1", required=False)

        args = parser.parse_args()

        self.svg_file_path = args.inputfile
        self.gcode_file_path = args.outputfile
        self.feedrate = int(args.feedrate)
        self.moverate = int(args.moverate)
        self.passes = int(args.passes)
        self.laserpower = ((float(args.laserpower.split("%")[0])/100.0))
        self.bedsizex =  int(args.bedsizex)
        self.bedsizey = int(args.bedsizey)
        self.smoothness = float(args.smoothness)

        if(self.laserpower > 1):
            print("[config] argument describing laser power is greater 100%")
            sys.exit(1)
        elif (self.laserpower < 0):
            print("[config] argument describing laser power is lower 0%")
            sys.exit(1)
        if (self.feedrate < 1):
            print("[config] argument describing feedrate is lower 1")
            sys.exit(1)
        if (self.moverate < 1):
            print("[config] argument describing moverate is lower 1")
            sys.exit(1)
        if (self.passes < 1):
            print("[config] argument describing passes is lower 1")
            sys.exit(1)
        if (self.bedsizex < 1):
            print("[config] argument describing bedsizex is lower 1")
            sys.exit(1)
        if (self.bedsizey < 1):
            print("[config] argument describing bedsizey is lower 1")
            sys.exit(1)




        






